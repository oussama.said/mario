using UnityEngine;
using UnityEngine.UI;

public class Mario : MonoBehaviour
{
    public FixedJoystick Joystick;
    public Text countertext;
    public Text countertext2;
    private int counter;
    private int counters;
    private float force = 16;
    private Animator anim;
    private Rigidbody2D rb;
    private void Start()
    {
        anim = GetComponent<Animator>();

        rb = GetComponent<Rigidbody2D>();
        counter = 0;
        counters = PlayerPrefs.GetInt("Coins", counter);

    }

    private void Update()
    {
        anim.SetFloat("speed", Joystick.Horizontal);
        transform.Translate(Joystick.Horizontal * Time.deltaTime * 10, 0f, 0f);
        PlayerPrefs.Save();
    }
    public void Jumpaction()
    {
        if (rb.velocity.y == 0f)
        {

            anim.SetTrigger("jump");
            rb.velocity = Vector2.up * force;
        }
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Coin")
        {
            counter++;
            Destroy(other);
            if (counter >= counters)
            {
                PlayerPrefs.SetInt("Coins", counter);
                PlayerPrefs.Save();
            }
            countertext.text = counter.ToString();
            countertext2.text = counters.ToString();


        }
    }
}
