using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UiAction : MonoBehaviour
{
    public Text Highscore;
    private int counters,counter;
    void Start()
    {
        counters = PlayerPrefs.GetInt("Coins", counter);
        Highscore.text = counters.ToString();
    }

    // Update is called once per frame
    void Update()
    {
        
    }
    public void gameoverscene()
    {
        PlayerPrefs.Save();
        SceneManager.LoadScene("Over");
    }
    public void replayscene()
    {
        PlayerPrefs.Save();
        SceneManager.LoadScene("SampleScene");
    }
    public void toMenu()
    {
        PlayerPrefs.Save();
        SceneManager.LoadScene("Menu");
    }

}
