using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;
using UnityEngine.SceneManagement;

public class Ennemies : MonoBehaviour
    
{
    Animator animator;
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Player")
        {
            PlayerPrefs.Save();
            Destroy(gameObject);
            SceneManager.LoadScene("Over");
        }
    }
    
}
